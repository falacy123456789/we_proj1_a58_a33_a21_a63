<html>
<head>
<link rel="stylesheet" type="text/css" href="../CSS/master.css">

    <link rel="stylesheet" type="text/css" href="../CSS/verticalScroling.css">
      <link rel="stylesheet" type="text/css" href="../CSS/fullwindow.css">
<link rel="stylesheet" type="text/css" href="../CSS/font.css">
    <script src="../css/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../CSS/jquery.carouFredSel-6.0.4-packed.js" type="text/javascript"></script>
    
    <script>
    $(function() {
	$('#carousel').carouFredSel({
		width: 900,
		height: '100%',
		direction: 'up',
		items: 1,
		scroll: {
			duration: 600,
			onBefore: function( data ) {
				data.items.visible.children().css( 'opacity', 0 ).delay( 200 ).fadeTo( 400, 1 );
				data.items.old.children().fadeTo( 400, 0 );
			}
		}
	});
});
    
    
    
    </script>
    
 <script>
        $(function() {
 
	var defaultCss = {
		width: 240,
		height: 150,
		marginTop: 50,
		marginRight: 25,
		marginLeft: 25,
		opacity: 0.5
	};
	var selectedCss = {
		width: 400,
		height: 260,
		marginTop: 0,
		marginRight: -25,
		marginLeft: -25,
		opacity: 1
	};
	var aniOpts = {
		queue: false,
		duration: 1000,
		easing: 'elastic'
	};
	var $car = $('#car');
	for ( var a = 0; a < 3; a++ ) {
		$car.prepend( '<div />' );
	}
	for ( var a = 0; a < 3; a++ ) {
		$car.append( '<div />' );
	}
	$car.find('img').css('zIndex', 1).css( defaultCss );
	$car.find('img').eq(0).css('zIndex', 2).css( selectedCss );
	$car.carouFredSel({
		circular: false,
		infinite: false,
		width: '100%',
		height: 250,
		items: 7,
		prev: '#prv',
		next: '#nxt',
		auto: false,
		scroll: {
			items: 1,
			duration: 1000,
			easing: 'elastic',
			onBefore: function( data ) {
				data.items.old.eq(3).find('img').css('zIndex', 1).animate( defaultCss, aniOpts );
				data.items.visible.eq(3).find('img').css('zIndex', 2).animate( selectedCss, aniOpts );
			}
		}
	});
});
        
        
        </script>
      
    
     <script>
       $(function() {
	$('#caro').carouFredSel({
		direction: 'up',
		auto: false,
		scroll: {
			duration: 1000
		},
		pagination: {
			container: '#pager',
			anchorBuilder: function() {
				return '<a href="#">'+ this.alt +'</a>';
			}
		}
	});
});
        </script>
    
    
    
      
</head>
<body>
<div id="container">

<div id="main-nav">
<img src="../images/logo.png" style="position:absolute;left:80px;top:0px;height:50px; width:150px; alt="Error";"/>
<img src="../images/ribon.png" style="position:absolute;left:250px;top:0px; width:50px;height:90px"/>
<img src="../images/white.png" style="position:absolute;left:230px;top:0px; width:20px;height:50px"/>
<ul class="nav-menu">
<li><a href="tv.html"/>TV</li>

<li><a href="contactuss.html"/>Email Us</li>
<li><a href="signup.html"/>Sign Up</li>
<li><a href="signin.html"/>Sign In</li>

</ul>


</div>

<div id="header">
<h4>MOVIEBOX ENTERTAINMENT</h4>
<div id="mnu">
<ul class="menu">

<li>Watch Online</li>
<li><a href="../images/sl/watchOnTv.html"/>Watch On TV</li>
<li>saved</li>
</ul>

</div>
</div>

<div id="contant">

<div id="cata">
<ul class="cata-menu">
<li>TV Shows</li>
<li>Movies</li>
<li>Sports</li>
<li><a href="kids.html"/>Family & Kids</li>
<li>Networks</li>
<li>Live TV</li>
<li>Lollywood</li>
<li>Bollywood</li>
</ul>

</div>
<div id="w-contant">
<div class="vertical">
    <div id="wrapper">
	<div id="carousel">
		<div class="item">
			<div>
				<img src="../images/cartoon/c1.jpg" width="300" height="240" border="0" />
				<div class="text">
					<h3>Delhi Safari</h3>
					<p>With their homes on the verge of destruction, jungle animals (Jason Alexander, Cary Elwes, Brad Garrett) travel to Delhi to ask humans a very important question.</p>
					<a href="mainPage.html" target="_blank">Visit site &raquo;</a>
				</div>
			</div>
		</div>
		<div class="item">
			<div>
				<img src="../images/cartoon/c2.jpg" width="300" height="240" border="0" />
				<div class="text">
					<h3>Monsters</h3>
					<p>Monsters Incorporated is the largest scare factory in the monster world, and James P. Sullivan (John Goodman) is one of its top scarers. Sullivan is a huge, intimidating monster with blue fur, large purple spots and horns. .</p>
					<a href="mainPage.html" target="_blank">Visit site &raquo;</a>
				</div>
			</div>
		</div>
		<div class="item">
			<div>
				<img src="../images/cartoon/c3.jpg" width="300" height="240" border="0" />
				<div class="text">
					<h3>Up</h3>
					<p>Carl Fredricksen (Ed Asner), a 78-year-old balloon salesman, is about to fulfill a lifelong dream. Tying thousands of balloons to his house, he flies away to the South American wilderness. But curmudgeonly Carl's worst nightmare comes true when he discovers a little boy named Russell is a stowaway.</p>
					<a href="mainPage.html" target="_blank">Visit site &raquo;</a>
				</div>
			</div>
		</div>
	</div>
</div>
    
    
    </div>
<hr>
    <div class="l">
    </div>
    <div class="fullwindow">

        <div id="wrappur">
	<div id="car">
		<div><img src="../images/cartoon/c4.jpg" width="240" height="150" /></div>
		<div><img src="../images/cartoon/c5.jpg" width="240" height="150" /></div>
		<div><img src="../images/cartoon/c6.jpg" width="400" height="250" /></div>
		<div><img src="../images/cartoon/d7.jpeg" width="400" height="250" /></div>
		<div><img src="../images/cartoon/d8.jpg" width="400" height="250" /></div>
	</div>
	<div id="nave">
		<a id="prv" href="#">&laquo; prev</a>
		<a id="nxt" href="#">next &raquo;</a>
	</div>
</div>
        
      
    </div>
    
    
    
      
        <div class="bt">
         <div id="wrappr">
	<div id="caro">
		<img src="../images/cartoon/d9.jpg" alt="Mickey Mouse" width="700" height="350" />
		<img src="../images/cartoon/d10.jpg" alt="Tangled" width="700" height="350" />
		<img src="../images/cartoon/d11.jpeg" alt="Fly Birds" width="700" height="350" />
	</div>
	<div id="pager"></div>
        
                

        
        </div>
    
    
    
    
    
</div>
</body>
</html>